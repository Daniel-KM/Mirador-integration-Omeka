const path = require('path');
const webpack = require('webpack');

module.exports = [
  {
    name: 'full',
    entry: './src/index.js',
    output: {
      filename: 'mirador-bundle.min.js',
      path: path.resolve(__dirname, 'dist'),
    },
    mode: 'production',
    devtool: "sourcemap",
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1,
      }),
    ],
  },
  {
    name: 'usual',
    entry: './src/index-usual.js',
    output: {
      filename: 'mirador-pack.min.js',
      path: path.resolve(__dirname, 'dist'),
    },
    mode: 'production',
    devtool: "sourcemap",
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1,
      }),
    ],
  },
];
